from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection


import json
import redis
import math


## find the nearest tenth additionally round
##
##
def find_nearest_tenth(value_given):
  initial = math.abs(value_given)
  down_score = 0
  up_score = 0
  for i in range(0, initial):
    if initial - i < 10:  
      down_score= initial-i
  for i  in range(initial, initial+10):
    if i>10:
      up_score= i-1
  return (initial-down_score) if down_score < up_score else (initial+up_score)
    
  
           



## minimal heap sort
# /# use for the search results

def swap(array,indice1,indice2):
  temp  =array[indice1]
  array[indice1] = array[indice2]
  array[indice2] = temp
  return array

## its a dictionary
def getarrval(array, indice):
  return array.rank

def heapsort(array, count):
  end  = count-1
  array = heapify(array, count)
  while end > 0:
    array = swap(array, end, 0)
    end = end -1
    array = shiftDown(array,0,end)
  return array


def shiftUp(array, start, end):
  child = end  
  while child > start:
    parent= int(math.floor((child-1)/2))
    parentrank = getarrval(array,parent)
    childrank = getarrval(array,child)

    if parentrank  > childrank:
      array =swap(array, parent, child)
      child = parent
    else:
      return array




def shiftDown(array, start, end):
  root = start
  while (root*2+1)  <=  end:
    child = root *  (2 +1)
    swp = root
    if array[swp] <  array[child]:
      swp = child
    swpval = getarrval(array,swp)
    swpval1 = getarrval(array,child+1)

    if ((child+1) <= end) and (swpval < swpval1):
      swp  = child+1 
    if swp == root:
      return array
    else:
      array = swap(array, root, swp)
      root =  swp
  return array

def heapify(a,count):
  start = math.floor((count-2)/2)
  while start >= 0:
     a = shiftDown(a, int(start), count - 1)
     start = start - 1

  return a


## connect to our database as well as redis perform before
## search engine
##
##
class SearchAuth(object):
  def __init__(self, db_db=Database, db_client=MongoClient, cache_client=redis):
    self.cache = cache_client
    self.configuration = json.loads(open("/usr/local/django_amd_backend/config.json","r").read())
    try:
      self.db = db_db( (self.configuration['mongo_host'], self.configuration['mongo_port']), db_client(unicode(self.configuration['mongo_db'])))
      self.cache = self.cache.StrictRedis( host=self.configuration['redis_host'], port=self.configuration['redis_port']  )
    except Exception, e:
      raise "Could not connect to the Mongo or Redis instance %s" % (e.__str__())

 

## SearchEngine should search the database for
## any things we need 
##
class SearchEngine(object):
  def __init__(self, userData, query='', list_of_filters=[]):
    self.auth = SearchAuth() 
    if self.auth:
      self.results = self.results_get(userData, query, list_of_filters)
    

  def get_results(self):
    return self.results
  ## make  a  python dictionary 
  ## accept the filters for the 
  ## following:
  ## dateCreated, dateModifed, description,
  ##

  def build_query(self,query, filters):
     return dict(initialQuery=query, filters=filters)

  ## filterNames+theQuery+userAgent+geolocationRadius
  def _get_pre_query_hash(self, query, filterNames, userAgent):
   geolocationServices = GeolocationServices()

   initialQuery = "lat_ " + geolocationServices._find_relative_lat(userAgent.getLat())
   initialQuery += "&lng_"+ geolocationServices._find_relative_lng(userAgent.getLng())
   initialQuery += "&query=" + query
   for i in filterNames.keys():
      initialQuery += i + "_"+filterNames[i]
   return initialQuery
   

 
  ## any pre logic to search 
  def _results_pre(self, userAgent, query,filters):
    queryHash = self._get_pre_query_hash(queryAgent,query,filters)

    results = self.auth.cache.get(queryHash)
    return results
    
  ## get the results
  def _results_get(self,query,filters):
       
    results = self.results_pre()
    self.auth.db.find("amd", 
    queries = self.build_query(query,filters))

    if not results:
      results = self.auth.db.find("amd", queries['initialQuery'])
     
    outputResults = [] 
    for i in results:
      sr = SearchResult(i)
      srank =  SearchRank(sr,filters)
      sr.attach_rank( srank )
      outputResults.append(sr)
    
    searchresults = heapsort(sr)
       

    self.results_post(searchresults)
    self.search_results = SearchResults(searchResults)
    self.json_results = json.dumps(self.search_results.as_dict())
  def as_json(self):
    return self.json_results
    

  ## any post logic to the search
  def _results_post(self,searchResults):
    queryHash = self._get_pre_query_hash(queryAgent,query,filters)
    status = self.auth.cache.set(queryHash, json.dumps(searchResults))
    return status



class SearchResult(object):
  def __init__(self, searchResult, filters):
    self.searchResult = searchResult
  def __getitem__(self,item):
    return selfattr(self,item)
  def attach_rank(self,searchRank):
    self.rank = searchRank.get_rank()   
  def as_dict(self):
    dictionary=dict()
    for i in self.attributes:
      dictionary[i]=getattr(self,i)
    return dictionary

class SearchResults(object):
  def __init__(self, searchResults):
     self.results = searchResults
  def as_dict(self):
    dictionaries = []
    for i in self.results:
      dictionary.append(i.as_dict())
    return dictionaries






## compute a search rank for a result given a set of
## filters and the result
class SearchRank(object):
  def __init__(self, searchResult, filters):
    self.matches = 0
    self.rank = 0 
    self.filters =[]
    for i in filters:
      for j in searchResult.keys():
        if i == j:
          self.rank +=self.compare_rank(searchResult[j], filters[i])


  def context_rules(self,contextName):
    rules =  dict(
        dateCreated=dict(thresholdPlus=True,thresholdMinus=True, threshold=10),
        dateModified=dict(thresholdPlus=True,thresholdMinus=True,  threshold=10),
        description=dict(wordMatch=true, maxWordLen=100, keyword=True),
        tags=dict( wordCount=true, tags=True )
    )
    ## these may not always have a rule
    if contextName in rules.keys():
      return rules[contextName]
    return False

  def  get_context_for(self,filterValue):
    context_rule = self.context_rules(filterValue)
    return context_rule

  def compare_threshold(self,option,difference):
    return threshold - difference
      
    

  def compare_word_match(self, searchValue, filterValue):
    matches = 0
    filterWords = self.get_words(filterValue)
    searchWords = self.get_words(searchValue)
    score = 0
    for i in range(0,len(filterWords)):
      for j in range(0, len(searchWords)):
        if self.word_alike(filterWords[i],searchWords[j]):
           
           relativeDifference= math.max(i,j)-math.min(i,j)
           score += 1*(filterWords-relativeDifference)
    return score
  def compare_tag_match(self, tagValue, filterValue):
    tags = 0
    score = 0
    for i in tagValues:
      for j in searchWords:
        match_of_tag =  self.word_alike(filterWords[i],searchWords[j])
        if match_of_tag == 1:
           score += 1
        elif match_of_tag == -1:
           score += .5
    return score
        

    
  ## compare two ranks. These should be the term and the value
  def compare_rank(self,searchValue, filterValue): 
    ## switch based on context
    context = self.get_context_for(filterValue)
    if  context: 
      if 'threshold' in context.keys():
        if context['thresholdMinus'] and context['tresholdPlus']:
          if math.min(searchValue, filterValue) == filterValue:
            return  self.compare_treshold(searchValue-filterValue)
          else:
            return self.compare_threshold(filterValue-searchValue)
      if 'keyword' in context.keys():
        if context['wordMatch']:
           return self.compare_word_count( searchValue, filterValue )
      if 'tags' in  context.keys():
        return self.compare_tag_match( filterValue, searchValue )
    return 0


class UserAgent(object):
  def getLat(self):
    return self.lat
  def getLng(self):
    return self.lng
   
class GeolocationServices(object):
  ## find the nearest oneth
  ## 55.0002020 -> 50

  def find_relative_lat(self,lat):
    return  find_nearest_tength(lng)
  def find_relative_lng(self,lng):
    return  find_nearest_tenth(lng)


 
