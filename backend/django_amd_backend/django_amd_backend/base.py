from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.cursor import CursorType
from bson.objectid import ObjectId
from elasticsearch.client import Elasticsearch
import  mwclient

import re
import json
import redis
import math



## minimal heap sort
# /# use for the search results

def swap(array,indice1,indice2):
  temp  =array[indice1]
  array[indice1] = array[indice2]
  array[indice2] = temp
  return array

## its a dictionary
def getarrval(array, indice):
  return array[indice].rank

def heapsort(array, count):
  end  = count-1
  array = heapify(array, count)
  while end > 0:
    array = swap(array, end, 0)
    end = end -1
    array = shiftDown(array,0,end)
  return array


def shiftUp(array, start, end):
  child = end  
  while child > start:
    parent= int(math.floor((child-1)/2))
    parentrank = getarrval(array,parent)
    childrank = getarrval(array,child)

    if parentrank  > childrank:
      array =swap(array, parent, child)
      child = parent
    else:
      return array




def shiftDown(array, start, end):
  root = start
  while (root*2+1)  <=  end:
    child = root *  (2 +1)
    swp = root
    swpval = getarrval(array,swp)
    childval =getarrval(array,child)
    if swpval < childval:
      swp = child
    swpval = getarrval(array,swp)
    swpval1 = getarrval(array,child+1)

    if ((child+1) <= end) and (swpval < swpval1):
      swp  = child+1 
    if swp == root:
      return array
    else:
      array = swap(array, root, swp)
      root =  swp
  return array

def heapify(a,count):
  start = math.floor((count-2)/2)
  while start >= 0:
     a = shiftDown(a, int(start), count - 1)
     start = start - 1

  return a

class Article(object):
	def __init__(self,articleData=dict(), articleContents=""):
		self.articleData=articleData
		self.articleContents = articleContents
	def  as_json(self):
		return dict(data=self.articleData,contents=self.articleContents)


## connect to our database as well as redis perform before
## search engine
##
##
class SearchAuth(object):
  def __init__(self, db_db=Database, db_client=MongoClient, cache_client=redis):
    self.cache = cache_client
    self.configuration = json.loads(open("/usr/local/django_amd_backend/config.json","r").read())
    if self.configuration['use_es']:
      try:
        self.use_es = True
        self.es = Elasticsearch([
             self.configuration['elasticsearch_url']
        ]) 
        if  not 'elasticsearch_index' in self.configuration.keys():
          ##create an index
          self.es.indices.create(index='django_index',ignore=400)
          self.configuration['elasticsearch_index']='django_index'
          file = open("/usr/local/django_amd_backend/config.json","w+")
          file.write(json.dumps(self.configuration))
          file.close()
      except Exception, e:
        print "An exception occured: " + e.__str__()
      
    else:
     
      try:
        connection = MongoClient( self.configuration['mongo_host'], int(self.configuration['mongo_port']) )
        try:
          self.db = Database(connection, unicode(self.configuration['mongo_db']))
          self.collection = getattr(self.db, self.configuration['mongo_collection'])
        except:
          try:
            self.db =Database(connection, unicode(self.configuration['mongo_db']))
            self.collection  = self.db.create_collection(self.configuration['mongo_collection'])
          except Exception, e:
            print "Could not create collection %s for MongoDB" % ( self.configuration['mongo_collection'] )
            raise e
        
        self.cache = self.cache.StrictRedis( host=self.configuration['redis_host'], port=int(self.configuration['redis_port']),db=0 )
      except Exception, e:
        print "Received an exception while trying to connect to Mongo And Redis %s" % ( e.__str__() )
        raise e
   

## SearchEngine should search the database for
## any things we need 
##
class SearchEngine(object):
  def __init__(self, user=dict(), query='', list_of_filters=dict(), limit=100,offset=0):
    self.auth = SearchAuth() 
    self.limit = limit
    self.offset = offset
    self.defaults =['All', 'None']
    self.per_page = 10
    self.get_fields()
    if self.auth:
      self.results = self._results_get(UserAgent(user), query, list_of_filters)

      

  def get_fields(self):
    data = json.loads(open("/usr/local/django_amd/fields.json","r").read())
    self.fields = data
    all_fields = json.loads(open("/usr/local/django_amd/fields_all.json","r").read())   
    self.all_fields = []
    for i in all_fields:
      for j in i['elementOptions']:
        self.all_fields.append(
		J
	 )



  

  def lookup_type(self, fieldId):
    for i in self.fields:
      for j  in i['elementOptions']:
        if j['id']==fieldId:
          if 'elementSubType' in dir(j):
            return j['elementSubType']
          else:
            return j['elementType']
    return ""


  def get_selects(self):
    selects = []
    for i in  self.filter_types.keys():
      if self.filter_types[i]=='select':
        selects.append( i  )
    return selects
  def get_results(self):
    return self.results
  ## make  a  python dictionary 
  ## accept the filters for the 
  ## following:
  ## dateCreated, dateModifed, description,
  ##

  #def build_query(self,query, filters):
  #   return dict(initialQuery=query, filters=filters)

  ## filterNames+theQuery+userAgent+geolocationRadius
  def _get_pre_query_hash(self, query, filterNames, userAgent):
   geolocationServices = GeolocationServices()
   initialQuery=""
   #initialQuery = "lat_ " + str(geolocationServices._find_relative_lat(userAgent.getLat()))
   #initialQuery += "&lng_"+ str(geolocationServices._find_relative_lng(userAgent.getLng()))
   initialQuery += "&query=" + query
   for i in filterNames.keys():
      if isinstance(filterNames[i],list):
        initialQuery += i +"_"+ filterNames[i].join(",")
      elif isinstance(filterNames[i],dict):
        initialQuery += i +"_" + self.join_dict(filterNames[i]) 
      elif isinstance(filterNames[i], int) or isinstance(filterNames[i],float):
        initialQuery += i+"_" + str(filterNames[i])
      elif isinstance(filterNames[i],str):
        initialQuery += i + "_"+ filterNames[i]
   return initialQuery
  
  def join_dict(self, the_dictionary):
    values = []
    for i in the_dictionary.keys():
      ## TODO add a recursive implemention for lists also consider the join function on lists it should all be serialized
      values.append(the_dictionary[i])
    return values.join(",")


  ## should return a key, value pair
  ## ordered with the operators used for matching
  ## 
  ## { 
  ##   "name": { $regex: ".*text.*" },
  ##  "date": { $regex: ".*text.*" } 
  ##  }
  def build_query(self, dictionary_filters):
    queries =  dict()
  
     
    """    
    for i in dictionary_filters.keys():
      type_of_filter = self.lookup_type(i)
      value = dictionary_filters[i]
      if type_of_filter:
        single_query = self.build_single_query( type_of_filter, value )
        if single_query:
          queries[i] =single_query
    """
    for i in self.all_fields:
      type_of_filter = self.lookup_type
      value =  dictionary_filters['all']
      if type_of_filter:
        single_query = self.build_single_query( type_of_filter, value )
   	if single_query:
          queries[i] = single_query
	
	 
      
    return queries 
    

  def ensure_value(self, filter_type, value):
    if value:
      if  value in self.defaults:
        return False
      if self.remove_spaces(value) == "":
        return False
      return True
    return False

  def build_single_query(self,type_filter, filter_value):
   
    find_val =  self.ensure_value(type_filter, filter_value) 
    if find_val:
      if  type_filter == "text"  or type_filter == "select":
        return {"$regex": ".*"+filter_value+".*"}
      elif type_filter  == "date":
        return  filter_value
      else:
        return filter_value
    return False

  def serialize(self, list_of_results):
    for i in range(0,len(list_of_results)):
      list_of_results[i]['_id'] = str(list_of_results[i]['_id']) 
    return list_of_results
        

  def _db_get(self, queries):
    per_page = self.per_page
    if self.auth.use_es:
    
      results = self.auth.es.index(index=self.auth.configuration['elasticsearch_index'], body={"any": queries['all'], "timestamp": datetime.now(), "from":  self.offset, "size": self.limit})
      count = self.auth.es.count(index=self.auth.configuration['elasticsearch_index'])
      count =int(count['count'])
      pages= int(math.floor(count/ per_page))
        
      return dict(results=results,count=count,pages=pages)
    else:
      cursor = self.auth.collection.find(queries).skip(self.offset).limit(self.limit) 
      count = self.auth.collection.count(queries) 
      per_page= self.per_page
      if count> 0 and count > per_page:
        pages = math.floor(count /  per_page)
      else:
        pages = 0

      return dict(results=self.serialize(list(cursor)),pages=int(pages),count=count)
  ## any pre logic to search 
  def _results_pre(self, userAgent, query,filters):
    queryHash = self._get_pre_query_hash(query,filters, userAgent)

    results = self.auth.cache.get(queryHash)

    return results
   

  def _try_json_parse(self, result_string): 
    try:
      results = json.loads(result_string)
    except Exception, e:
      results = False
    return results
    
  ## get the results
  def _results_get(self,userData,query,filters):
       
    #results = self._results_pre(  userData, query, filters )
    results = None
      
    if not results:
      full_query = self.build_query(filters)
      results = self._db_get( full_query )
       
      outputResults = [] 
      #for i in results:
      #  sr = SearchResult(i, filters)
      #  srank =  SearchRank(sr,filters)
      #  sr.rank = srank.rank
      #  outputResults.append(sr)
      #len_of_results = len(outputResults) 
      #if len_of_results>0:
      #  searchresults = heapsort(outputResults, len(outputResults))
      #else:
      #  searchresults = outputResults
      self.json_results = json.dumps(results)
      # self._results_post(userData, query, filters)
    else:
      self.json_results = json_dumps(results)


  def as_json(self):
    return self.json_results
  def remove_spaces(self,val):
    return re.sub("\s+","",val)
    

  ## any post logic to the search
  def _results_post(self,searchResults,userAgent,query,filters):
    queryHash = self._get_pre_query_hash( query, filters, userAgent)
    status = self.auth.cache.set(queryHash, searchResults)
    return status



class SearchResult(object):
  def __init__(self, searchResult, filters):
    self.keys = []
    for i in searchResult.keys():
      self.keys.append(i)
      setattr(self,i,searchResult[i])
    self.searchResult = searchResult
  def keys(self):
    return self.keys
  def __getitem__(self,item):
    return selfattr(self,item)
  def attach_rank(self,searchRank):
    self.rank = searchRank.get_rank()   
  def as_dict(self):
    dictionary=dict()
    for i in self.keys():
      dictionary[i]=getattr(self,i)
    return dictionary

class SearchResults(object):
  def __init__(self, searchResults):
     self.results = searchResults
  def as_dict(self):
    dictionaries = []
    for i in self.results:
      dictionary.append(i.as_dict())
    return dictionaries






## compute a search rank for a result given a set of
## filters and the result
##
## filters should look like
## {
##   "filter_1" :  "value"
##   "filter_2" :  "value"
##   "filter_3" : "value"
## }
## search result
## {
##   "filter_1": "value"
##   "filter_2": "value"
##   "filter_3": "value"
## }
##

class SearchRank(object):
  def __init__(self, searchResult, filters):
    self.matches = 0
    self.rank = 0 
    self.filters =[]
    for i in filters.keys():
      for j in searchResult.keys():
        if i == j:
          self.rank +=self.compare_rank(searchResult[j], filters[i], i)


  def context_rules(self,contextName):
    rules =  dict(
        dateCreated=dict(thresholdPlus=True,thresholdMinus=True, threshold=10),
        dateModified=dict(thresholdPlus=True,thresholdMinus=True,  threshold=10),
        description=dict(wordMatch=true, maxWordLen=100, keyword=True),
        tags=dict( wordCount=true, tags=True )
    )
    ## these may not always have a rule
    if contextName in rules.keys():
      return rules[contextName]
    return False

  def  get_context_for(self,filterValue):
    context_rule = self.context_rules(filterValue)
    return context_rule

  def compare_threshold(self,option,difference):
    return threshold - difference
      

  def  get_words(self, value_given):   
    return value_given.split(" ")

  def  word_alike(self, word1, word2):
     expr1 = re.compile(word1)
     return re.findall(expr1, word2)
  def word_match(self,word1, word2):
    return word1 == word2
  


  def compare_word_match(self, searchValue, filterValue):
    matches = 0
    filterWords = self.get_words(filterValue)
    searchWords = self.get_words(searchValue)
    score = 0
    for i in range(0,len(filterWords)):
      for j in range(0, len(searchWords)):
        if self.word_alike(filterWords[i],searchWords[j]):
           
           relativeDifference= math.max(i,j)-math.min(i,j)
           score += 1*(filterWords-relativeDifference)
    return score
  
  def compare_tag_match(self, tagValue, filterValue):
    tags = 0
    score = 0
    for i in tagValues:
      for j in searchWords:
        match_of_tag =  self.word_alike(filterWords[i],searchWords[j])
        if match_of_tag == 1:
           score += 1
        elif match_of_tag == -1:
           score += .5
    return score
        

    
  ## compare two ranks. These should be the term and the value
  def compare_rank(self,searchValue, filterValue, filterName=''): 
    ## switch based on context
    context = self.get_context_for(filterName)
    if  context: 
      if 'threshold' in context.keys():
        if context['thresholdMinus'] and context['tresholdPlus']:
      
          if math.min(searchValue, filterValue) == filterValue:
            if  (searchValue-filterValue)<context['threshold']:
              return  self.compare_threshold(searchValue-filterValue)
          else:
            if (filterValue-searchValue)<context['treshold']:
              return self.compare_threshold(filterValue-searchValue)
      if 'keyword' in context.keys():
        if context['wordMatch']:
           return self.compare_word_match( searchValue, filterValue )
      
    return 0


class UserAgent(object):
  def __init__(self,userObject):
    for i in userObject.keys():
      setattr(self,i,userObject[i])
    
  def getLat(self):
    return self.lat
  def getLng(self):
    return self.lng
   
class GeolocationServices(object):
  ## find the nearest oneth
  ## 55.0002020 -> 50

  def _find_relative_lat(self,lat):
    return  None
  def _find_relative_lng(self,lng):
    return   None
  def find_user_country_lat_lng(self):
    return ""
  def find_user_city_lat_lng(self):
    return ""


 
