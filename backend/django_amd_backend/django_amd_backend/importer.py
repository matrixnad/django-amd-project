
## import any data from the  Aviation.xml file into ourdatabase
## make the data json
from  base import SearchAuth
import xml.etree.ElementTree as ET
import json
import mwclient
from mwclient.page import Page
searchAuth = SearchAuth()
#fileContents = open("./AviationData.xml", "r").read()
config =  json.loads(open("/usr/local/django_amd_backend/config.json").read())
tree   = ET.parse("./AviationData.xml")
mw = mwclient.login(config['mwclient_username'], config['mwclient_password'])
site = mw.Site(config['mwclient_site'])
ARTICLE = """
"""

class  DjangoAmdImporter(object):
	def __init__(self,db=None,mw=None):
		global ARTICLE
		self.db=db
		self.mw = mw
		self.config = json.loads(open("/usr/local/django_amd_backend/config.json").read())
		self.articleTemplate = ARTICLE
	def camelCase(self, letters ):
		return letters[0:1].lower()  +letters[1:len(letters)]

	def makeOurStructure(self, struct ):
		newStruct = dict()
		for i in struct.keys():
			newStruct[ self.camelCase(i) ] = struct[i]
		return newStruct
	def makeStructureTitle(self,structure):
		return structure[self.titleKey]
	def makeStructureArticle(self,structure):
		initialArticle = self.articleTemplate
		for i in self.articleRs:
			initialArticle  = re.sub( "{"  + i + "}", structure[i], initialArticle)
		return initialArticle
		
		return ""

	def _insertLogic(self,structure):
		newRecord = self.es.index(index=self.config['elasticsearch_index'], id=None, doc_type=self.config['elasticsearch_doc_type'], body=structure)
		return structure
	def  getCurrentRecord(self,structure):
		searchQuery = self.getSearchQuery(structure)
		result = self.es.search(index=self.config['elasticsearch_index'], body={'all': searchQuery})
		if  self.config['elasticsearch_search_key'] in dir(result):
			lenOfResults = result[self.config['elasticsearch_search_key']][self.config['elasticsearch_search_key']]
			if lenOfResults > 0:
				return  result[self.config['elasticsearch_search_key']][self.config['elasticsearch_search_key']][0]
		return False
			
	def insertLogic( structure ):
	  currentRecord = self.getCurrentRecord(structure)
	  if not currentRecord:
		currentRecord = self._insertLogic(structure) 
	  title = self.makeStructureTitle(structure)
	  article = self.makeStructureArticle(structure)
	  newMediaWikiPage =  Page(title) 
	  currentText = newMediaWikiPage.text()
	  if currentText != "":
		newMediaWikiPage.edit(text=article)
		newMediaWikiPage.save()


cli = DjangoAmdImporter(db=searchAuth, mw=mw)
root = tree.getroot()
for i in root:
	for j in i:
		structure = cli.makeOurStructure(j.attrib)
		print "Inserting %s into `results`"  % (structure.__str__())
		resultOfInsert = cli.insertLogic(structure)
