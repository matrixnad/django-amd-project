from setuptools import setup
import subprocess
import sys
import os


setup(
   name="django_amd_backend",
   description="Backend bindings to the AMD application",
   classifiers=[],
   packages=['django_amd_backend'] 
) 


if sys.argv[1] == "install":
  if os.path.isdir("/usr/local/django_amd_backend"):
    pass
  else:
    os.makedirs("/usr/local/django_amd_backend/")

  parentdir = os.getcwd() +"/../"
  subprocess.call(['pip', 'install','-r', parentdir+"requirements.txt"])




