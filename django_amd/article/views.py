from layout  import  ArticleLayout,JSONLayout

from  django_amd_backend import Editor,Article
from django_amd_backend.importer import DjangoAmdImporter

from  elasticsearch.client import Elasticsearch
import mwclient
import json
STATUS_CODES = { 
	 "OK": 1,
	"ERROR": 2 
}
def response_json(status,data=None):
  return json.dumps(dict(status=status,message=data))
def get_config():
  config = json.loads(open("/usr/local/django_amd_backend/config.json").read())
  return config
def  connect_auth():
  config = get_config()
  es = Elasticsearch([
	config['elasticsearch_url']
   ])
  return es

def connect_mw():
  config = json.loads(open("/usr/local/django_amd_backend/config.json").read())
  mwClient = mwclient.login(config['mw_username'],config['mw_password'])
 
def view_article(request):
  mwClient = connect_mw()
  return al.render_layout(request, "article/view_article.html", { })

def get_article(request):
  mwClient = connect_mw()
  auth =  connect_auth() 
  config = get_config()
  result = auth.search(index=config['elasticsearch_index'], body={"_id": request.id})
  page = mwClient.Pages[result[config['elasticsearch_main_key']]]
  textOfPage = page.text()
  article = Article(result, textOfPage)
  jl = JSONLayout()
  return jl.render_layout(request,"article/get_article.html",{
	"content": article.as_json()
  })

  


def edit_article(request):
  global STATUS_CODES
  mwClient = connect_mw()
  esClient = connect_auth()
  configuration =get_config()
  article =    esClient.get(index=configuration['elasticsearch_index'], id=request['id'])
  page = mwClient.Pages[request.get("page_name")]
  field_keys = get_field_keys()
  for i in request.keys():
    if not i  == "id" and i in  field_keys:
      article[i] = request[i]
   

  statusUpdate = esClient.update(index=configuration=['elasticsearch_index'],id=request['id'],params=article)
  djangoAmdImporter = DjangoAmdImporter()
  newPageContents = djangoAmdImporter.makeArticleStructure(article)
  currentPageContents = page.text()
  page.edit(text=newPageContents)
  result_of_save = page.save()
  jl = JSONLayout()
  if result_of_save:
     return jl.render_template("article/edit_article.html", {
  	 "content": response_json(STATUS_CODES['OK'], dict())
	 })
  return jl.render_template("article/edit_article.html", {
	"content": response_json(STATUS_CODES['ERROR'],dict())
	})

     
  

