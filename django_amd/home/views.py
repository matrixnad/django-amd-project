# Create your views here.
from django.http.response import HttpResponse
from  django.template import loader,RequestContext
from django.views.generic.base import TemplateView
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from layout import MainLayout





## goto  AMD_impl
## main segment
@csrf_exempt
def home(request):
  mainLayout = MainLayout()
  render = loader.get_template("main/home.html")
  context =  {
      "title":"Aviation Misaps Database",
      "description":"The Aviation Mishaps Database"
   }
  return HttpResponse(content= render.render( context ) )
def about(request): 
  context = RequestContext(request, {
      "title":"About Aviation Mishaps Database"
  })
  return render_to_response("main/about.html",context)
def contact(request):
  context = RequestContext(request, {
      "title":"Contact Us",
      "email":"about@aviationsmishapsdatabase.com"
  })
  return render_to_response("main/contact.html", context)




