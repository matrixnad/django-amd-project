
from django.db.models.base import Model

class SearchResult(Model):
  title = ""
  description = ""
  tags = []
  posted = "" 
  modified = ""

