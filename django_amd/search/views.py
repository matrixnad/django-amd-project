from django.views.generic.base import View 
from django.http.response import HttpResponse
from django.template import loader, RequestContext
from django.shortcuts import render_to_response

from models import SearchResult
from django_amd_backend import SearchEngine
from django.views.decorators.csrf import csrf_exempt
from layout import MainLayout,SearchLayout,JSONLayout
import json


@csrf_exempt
def index(request):
  searchLayout = SearchLayout()

  return searchLayout.render_template(request, "search/index.html",{})
 
@csrf_exempt
def results(request): 
  searchLayout = SearchLayout()
  return searchLayout.render_template(request, "search/results.html", {})

@csrf_exempt
def get_results(request):
    
  json_data = json.loads(request.body)
  userData = json_data['user']
  query = json_data['query']
  filters = json_data['filters']
  offset = json_data['offset']
  limit = json_data['limit']
  print "Incoming Query is: %s" % ( json.dumps(json_data)) 
  results = SearchEngine(user=userData,query=query,list_of_filters=filters,offset=offset,limit=limit)
    
  as_json = results.as_json()
  renderer = RequestContext(request, {
      "results": as_json
  })
  template = loader.get_template("search/get_results.html")
  return HttpResponse(content=template.render( renderer), content_type='application/json')
@csrf_exempt 
def get_fields(request):
  
  file_of_fields = "/usr/local/django_amd/fields.json"
  data_fields = json.loads(open(file_of_fields,"r+").read())
  jsonLayout = JSONLayout()

  return jsonLayout.render_template(request, "search/get_fields.html", {
     "results": json.dumps(data_fields)
  }, content_type='application/json')




