#! /usr/bin/env python2.7
"""{{ project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from home.views import home
from search.views import results,index,get_results,get_fields

urlpatterns = [
    # Homepage
    url(r'^home$', home, name='home'),
    url(r'^search$', index, name='search'),
    url(r'^search/results', results, name='results'),
    url(r'^search/get_results', get_results,name='get_results'),
    url(r'^search/get_fields',  get_fields, name='get_fields'),
    url(r'^article/get_article', get_article, name='get_article'),
    url(r'^article/view_article', view_article, name='view_article'),
    url(r'^article/edit_article', edit_article, name='edit_article')
]

